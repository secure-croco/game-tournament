$ErrorActionPreference = "Stop"
$ProgressPreference = "SilentlyContinue"

. $PSScriptRoot/toolset_audio_speech.ps1
. $PSScriptRoot/toolset_display_ascii.ps1
. $PSScriptRoot/toolset_display_round.ps1
. $PSScriptRoot/toolset_io_config_file.ps1
. $PSScriptRoot/toolset_logic_pools.ps1

Clear-Host

$global:SpeechSynthesizer = Initialize-SpeechSynthesizer
$global:AnnounceColor = "Red"
$global:TeamColor = "Cyan"
$global:GameColor = "Green"
$global:TeamPool = @()
$global:GamePool = @()
$global:TeamThatDidNotPlay = $null
$global:Round = 1

while($true)
{
    Reset-TeamPool
    Reset-GamePool

    Show-TeamPool
    Show-GamePool

    Read-Host "(press ENTER to begin Round $global:Round)"
    Show-Round
    Show-ChosenTeams
    Show-ChosenGame

    $global:Round++
}
