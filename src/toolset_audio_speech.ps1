Add-Type -AssemblyName System.Speech

function Initialize-SpeechSynthesizer()
{
    $speech = New-Object System.Speech.Synthesis.SpeechSynthesizer

    $voiceList = $speech.GetInstalledVoices().VoiceInfo.Name
    
    if($voiceList.Count -eq 0)
    {
        Write-Host "No text-to-speech voices installed on this system. Voice will be disabled."
        $speech = $null
    }
    else
    {
        $preferredVoice = "Microsoft Zira Desktop"

        if($voiceList.Contains($preferredVoice))
        {
            $speech.SelectVoice($preferredVoice)
        }

        $speech.SetOutputToDefaultAudioDevice()
    }

    return $speech
}
