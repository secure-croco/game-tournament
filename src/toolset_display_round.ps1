function Show-Round()
{
    Show-AsciiArt -Text "ROUND $global:Round" -Color $global:AnnounceColor
    Start-Sleep -Milliseconds 1000
}

function Show-ChosenTeams()
{
    $team1 = Select-RandomTeam
    $team2 = Select-RandomTeam

    Show-AsciiArt -Text $team1   -Color $global:TeamColor
    Show-AsciiArt -Text "versus" -Color $global:TeamColor
    Show-AsciiArt -Text $team2   -Color $global:TeamColor
    Start-Sleep -Milliseconds 1000
}

function Show-ChosenGame()
{
    $game = Select-RandomGame
    
    Show-AsciiArt -Text "The chosen game is..." -Color $global:AnnounceColor
    Start-Sleep -Milliseconds 1000
    Show-AsciiArt -Text $game -Color $global:GameColor
}
