function Read-ConfigPool([string] $ConfigFile)
{
    $list = New-Object System.Collections.ArrayList
    $lines = [IO.File]::ReadAllLines($ConfigFile)
    $lines = $lines | Where-Object { -not [string]::IsNullOrWhiteSpace($_) }
    $list.AddRange($lines)
    Write-Output $list -NoEnumerate
}
