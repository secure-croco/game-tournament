function Select-RandomTeam()
{
    $selectedTeam = $null
    
    if($global:TeamThatDidNotPlay)
    {
        $selectedTeam = $global:TeamThatDidNotPlay
        $global:TeamThatDidNotPlay = $null
    }
    else
    {
        $selectedTeam = $global:TeamPool | Get-Random
    }
    
    $global:TeamPool.Remove($selectedTeam)
    return $selectedTeam
}

function Select-RandomGame()
{
    $selectedGame = $global:GamePool | Get-Random
    $global:GamePool.Remove($selectedGame)
    return $selectedGame
}

function Reset-TeamPool()
{
    $MinTeamPoolSize = 2

    if($global:TeamPool.Count -lt $MinTeamPoolSize)
    {
        if($global:TeamPool.Count -eq 1)
        {
            $global:TeamThatDidNotPlay = $global:TeamPool | Select-Object -First 1
        }

        $global:TeamPool = Read-ConfigPool "config/team_pool.conf"
        Write-Host "Team pool has been replenished!"
        Write-Host ""
    }
}

function Reset-GamePool()
{
    $MinGamePoolSize = 1

    if($global:GamePool.Count -lt $MinGamePoolSize)
    {
        $global:GamePool = Read-ConfigPool "config/game_pool.conf"
        Write-Host "Game pool has been replenished!"
        Write-Host ""
    }
}

function Show-TeamPool()
{
    Write-Host "Remaining teams in pool:" -ForegroundColor $global:TeamColor
    Write-Host "------------------------" -ForegroundColor $global:TeamColor
    $global:TeamPool | Write-Host
    Write-Host ""
}

function Show-GamePool()
{
    Write-Host "Remaining games in pool:" -ForegroundColor $global:GameColor
    Write-Host "------------------------" -ForegroundColor $global:GameColor
    $global:GamePool | Write-Host
    Write-Host ""
}
